import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import math
from sklearn.metrics import mean_squared_error, r2_score
from sklearn.preprocessing import MinMaxScaler
from keras.models import Sequential
from keras.layers import Dense, LSTM, Dropout  

#dataset
G = pd.read_excel('all_G1.xlsx') #change the excel file to predict for all_G1, all_G2, all_G3, all_G4, all_G5
G_train = G.iloc[:,-1].values

#scaling
scale_G = MinMaxScaler(feature_range = (0,1))
train_scale = scale_G.fit_transform(G_train.reshape(-1,1))
#print(train_scale.shape)

#recursion of data
train_input = []
train_output = []
for i in range(50, train_scale.shape[0]): #you can increase the number of selected training rows >50
    train_input.append(train_scale[i-50:i,0:21])
    train_output.append(train_scale[i,0:21])
train_input, train_output = np.array(train_input), np.array(train_output)

#print (train_input)
#print (train_output)
#print (train_input.shape)
#print (train_output.shape)

train_input = np.reshape(train_input, (train_input.shape[0], train_input.shape[1], 1))

#Initialising
model = Sequential()
#first input layer
model.add(LSTM(units = 50, return_sequences = True, input_shape = (train_input.shape[1], 1)))
model.add(Dropout(0.2))
#second input layer
model.add(LSTM(units = 50, return_sequences = True))
model.add(Dropout(0.2))
#third input layer
model.add(LSTM(units = 50, return_sequences = True))
model.add(Dropout(0.2))
#fourth input layer
model.add(LSTM(units = 50, return_sequences = False))
model.add(Dropout(0.2))
 
#output layer
model.add(Dense(1, activation= 'linear'))
model.compile(optimizer= "adam", loss = "mse", metrics= ['accuracy'])
model.fit(train_input, train_output, epochs = 10, batch_size= 4)
#you can increase the epoch to 50...it was reduced to enable fast execution
#increase batch_size to 16, 32, etc.

#testing the data
test_G = pd.read_excel("test_G1.xlsx") #change the excel file to predict for test_G1, test_G2, test_G3, test_G4, test_G5
G_test = test_G.iloc[:,-1].values
test_scale = scale_G.transform(G_test.reshape(-1,1))

dataset_total = np.concatenate((G_train, G_test), axis=0)
input = dataset_total[len(dataset_total) - len(G_test) - 50:].reshape(-1,1)
input = scale_G.transform(input)
print('input shape:', input.shape)

test_input = []
for i in range(50, input.shape[0]):
    test_input.append(input[i-50:i,0:21])
test_input = np.array(test_input)
test_input = np.reshape(test_input, (test_input.shape[0], test_input.shape[1], 1))
predict_stress_value = model.predict(test_input)
predict_stress_value = scale_G.inverse_transform(predict_stress_value)

#statistical distribution
RMSE = math.sqrt(mean_squared_error(G_test, predict_stress_value))
r2 = r2_score(G_test, predict_stress_value)

#data visualisation
plt.plot(G_test, color = 'green', label = 'real stress value')
plt.plot(predict_stress_value, color = 'blue', label = 'predicted stress value')
plt.title('Roof & Ground Stress Ratio')
plt.legend()

model.summary()
print (f"{RMSE: .4f}")
print (r2)
plt.show()
