import numpy as np
from input_mod import stress_prediction

#input test parameters
'''density, 
yound_modulus, 
poisson, 
cohesion, 
friction_angle, 
tensile, 
distance'''
print('INPUT STOPE PARAMATERS')
m = float(input('M factor:    '))
c = float(input('C factor:    '))
distance = float(input('Input stope distance:  '))

print ('\nInput Parameters for laterite:')
density_L = float(input('Density:   '))
young_L = float(input('Young modulus:  '))
poisson_L = float(input('poisson ratio: '))
cohesion_L = float(input('Cohesion: '))
friction_L = float(input('Friction angle: '))
tensile_L = float(input('Tensile: '))

print ('\nInput Parameters for shale:')
density_S = float(input('Density:   '))
young_S = float(input('Young modulus:  '))
poisson_S = float(input('poisson ratio: '))
cohesion_S = float(input('Cohesion: '))
friction_S = float(input('Friction angle: '))
tensile_S = float(input('Tensile: '))

print ('\nInput Parameters for coal:')
density_C = float(input('Density:   '))
young_C = float(input('Young modulus:  '))
poisson_C = float(input('poisson ratio: '))
cohesion_C = float(input('Cohesion: '))
friction_C = float(input('Friction angle: '))
tensile_C = float(input('Tensile: '))

#predict value
user_input = np.array([[m,c,density_L,density_S,density_C,young_L,young_S,young_C,poisson_L,poisson_S,poisson_C,cohesion_L,cohesion_S,cohesion_C,friction_L,friction_S,friction_C,tensile_L,tensile_S,tensile_C,distance]])
'''sample of displayed input: 8, 6, 19.6, 21, 10.7, 120000, 980000, 550000, 0.3, 0.32, 0.3, 50.3, 208.2, 232.5, 32.8, 31.5, 27.8, 57, 3280, 3380,125.25'''
#user_input = ([[8, 6, 19.6, 21, 10.7, 120000, 980000, 550000, 0.3, 0.32, 0.3, 50.3, 208.2, 232.5, 32.8, 31.5, 27.8, 57, 3280, 3380,125.25]])
value = stress_prediction(user_input)
print('Predicted stress value = ', value)