import numpy as np
import pandas as pd
from sklearn.preprocessing import MinMaxScaler
from keras.models import Sequential
from keras.layers import LSTM, Dropout, Dense

def stress_prediction(user_input):
    # Load the dataset
    G = pd.read_excel('all_G1.xlsx') #change the excel file to predict for G1, G2, G3, G4, G5 parameters
    x = G.iloc[:, :21].values
    y = G.iloc[:, -1].values

    # Scale the input data
    sc = MinMaxScaler(feature_range=(0, 1))
    x_scaled = np.zeros_like(x)
    for i in range(x.shape[1]):
        x_scaled[:, i] = sc.fit_transform(x[:, i].reshape(-1, 1)).flatten()
    y_scaled = sc.fit_transform(y.reshape(-1, 1))

    # Reshape the input data for LSTM
    x_reshaped = np.reshape(x_scaled, (x_scaled.shape[0], 1, x_scaled.shape[1]))

    # Build the LSTM model
    model = Sequential()
    model.add(LSTM(units=50, activation='tanh', return_sequences=True, input_shape=(1, x_scaled.shape[1])))
    model.add(Dropout(0.2))
    model.add(LSTM(units=50, activation='tanh', return_sequences=True))
    model.add(Dropout(0.2))
    model.add(LSTM(units=50, activation='tanh', return_sequences=False))
    model.add(Dropout(0.2))
    model.add(Dense(units=1))

    # Compile and train the model
    model.compile(optimizer='adam', loss='mean_squared_error')
    model.fit(x_reshaped, y_scaled, batch_size=32, epochs=100)

    # Prepare the test input data
    test_input = np.array(user_input)
    test_input_scaled = np.zeros_like(test_input)
    for i in range(test_input.shape[1]):
        test_input_scaled[:, i] = sc.transform(test_input[:, i].reshape(-1, 1))
    test_input_reshaped = np.reshape(test_input_scaled, (test_input_scaled.shape[0], 1, test_input_scaled.shape[1]))

    # Predict the stress value
    y_pred_scaled = model.predict(test_input_reshaped)
    stress_value = sc.inverse_transform(y_pred_scaled)
    return (stress_value)
